
<div class="row">

	<!-- EVENT CALENDAR OF CURRENT MONTH -->
	<div class="col-md-12">   
        <div class="panel panel-primary " data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="fa fa-calendar"></i>
                    <?php echo get_phrase('calendar');?> (this Month)
                </div>
            </div>
            <div class="panel-body" style="padding:0px;">
                <div class="calendar-env">
                    <div class="calendar-body">
                        <div id="event_calendar"></div>
                    </div>
                </div>
            </div>
        </div>
	</div>
	
</div>



<script>
  $(document).ready(function() {
	  
		  var calendar = $('#event_calendar');
            calendar.fullCalendar({
            header: {
            left: 'title',
                    right: 'month,agendaWeek,agendaDay today prev,next'
			},
			
			defaultView: 'month',
			
			editable: true,
			height: 700,
			droppable: true,

			events:
	        [
	            <?php
	            	$this->db->where('user_type' , $this->session->userdata('login_type'));
	            	$this->db->where('user_id' , $this->session->userdata('login_user_id'));
	            	$events = $this->db->get('calendar_event')->result_array();
	            	foreach ($events as $row):
	            ?>
	                {
	                    title   :   "<?php  echo $row['title'];?>",
	                    start   :   new Date(<?php echo date('Y', $row['start_timestamp']); ?>, 
	                                    <?php echo date('m', $row['start_timestamp']) - 1; ?>, 
	                                    <?php echo date('d', $row['start_timestamp']); ?>),
	                    end    :   new Date(<?php echo date('Y', $row['end_timestamp']); ?>, 
	                                    <?php echo date('m', $row['end_timestamp']) - 1; ?>, 
	                                    <?php echo date('d', $row['end_timestamp']); ?>),
	                    allDay: true,
	                    id: "<?php echo $row['calendar_event_id'];?>",
	                    color: "<?php echo $row['colour'];?>"
	                },
	            <?php endforeach ?>
	        ],

			
			drop: function(date, allDay) {
				
				var $this = $(this),
					eventObject = {
						title: $this.text(),
						start: date,
						allDay: allDay,
						className: $this.data('event-class')
					};
					
				calendar.fullCalendar('renderEvent', eventObject, true);
				
				$this.remove();
			}
		});

	});	
  </script>

<style type="text/css">
	.calendar-env .calendar-body .fc-header .fc-header-right {  
   padding: 0px; 
  text-align: right;
}
.calendar-env .calendar-body .fc-header .fc-header-left {  
  padding: 0px; 
}
</style>


