<?php include 'inc/config.php'; ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Home Carousel -->
<div id="home-carousel" class="carousel carousel-home slide" data-ride="carousel" data-interval="5000">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="active item">
            <section class="site-section site-section-light site-section-top themed-background-default">
                <div class="container">
                    <h1 class="text-center animation-slideDown hidden-xs"> A complete web solution for Attorneys and Clients</h1>
                    <h2 class="text-center animation-slideUp push hidden-xs">SLOGAN</h2>
                    <p class="text-center animation-fadeIn">
                        <img src="img/placeholders/screenshots/" alt="Promo Image 1">
                    </p>
                </div>
            </section>
        </div>
        <div class="item">
            <section class="site-section site-section-light site-section-top themed-background-fire">
                <div class="container">
                    <h1 class="text-center animation-fadeIn360 hidden-xs">Featuring </h1>
                    <h2 class="text-center animation-fadeIn360 push hidden-xs">Letting you focus on connecting _________</h2>
                    <p class="text-center animation-fadeInLeft">
                        <img src="img/placeholders/screenshots/" alt="Promo Image 2">
                    </p>
                </div>
            </section>
        </div>
        <div class="item">
            <section class="site-section site-section-light site-section-top themed-background-amethyst">
                <div class="container">
                    <h1 class="text-center animation-hatch hidden-xs">Get Started now.....</h1>
                    <h2 class="text-center animation-hatch push hidden-xs">slogan</h2>
                    <p class="text-center animation-hatch">
                        <img src="img/placeholders/screenshots/" alt="Promo Image 3">
                    </p>
                </div>
            </section>
        </div>
        <div class="item">
            <section class="site-section site-section-light site-section-top themed-background-modern">
                <div class="container">
                    <h1 class="text-center animation-fadeInLeft hidden-xs">Tons of _____</h1>
                    <h2 class="text-center animation-fadeInRight push hidden-xs">Everything you need</h2>
                    <p class="text-center animation-fadeIn360">
                        <img src="img/placeholders/screenshots/" alt="Promo Image 4">
                    </p>
                </div>
            </section>
        </div>
    </div>
    <!-- END Wrapper for slides -->

    <!-- Controls -->
    <a class="left carousel-control" href="#home-carousel" data-slide="prev">
        <span>
            <i class="fa fa-chevron-left"></i>
        </span>
    </a>
    <a class="right carousel-control" href="#home-carousel" data-slide="next">
        <span>
            <i class="fa fa-chevron-right"></i>
        </span>
    </a>
    <!-- END Controls -->
</div>
<!-- END Home Carousel -->

<!-- Action -->

<!-- END Action -->

<!-- Promo #1 -->
<section class="site-content site-section site-slide-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <img src="img/placeholders/screenshots/" alt="Promo #1" class="img-responsive">
            </div>
            <div class="col-sm-6 col-md-5 col-md-offset-1 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <h3 class="h2 site-heading site-heading-promo">Feature 1</h3>
                <p class="promo-content"> <a href="features.php">Learn More..</a></p>
            </div>
        </div>
        <hr>
    </div>
</section>
<!-- END Promo #1 -->

<!-- Promo #2 -->
<section class="site-content site-section site-slide-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-5 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <h3 class="h2 site-heading site-heading-promo"> </h3>
                <p class="promo-content"><a href="features.php">Learn More..</a></p>
            </div>
            <div class="col-sm-6 col-md-offset-1 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <img src="img/placeholders/screenshots/" alt="Promo #2" class="img-responsive">
            </div>
        </div>
        <hr>
    </div>
</section>
<!-- END Promo #2 -->

<!-- Promo #3 -->
<section class="site-content site-section site-slide-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <img src="img/placeholders/screenshots/" alt="Promo #3" class="img-responsive">
            </div>
            <div class="col-sm-6 col-md-5 col-md-offset-1 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <h3 class="h2 site-heading site-heading-promo"></h3>
                <p class="promo-content"> <a href="features.php">Learn More..</a></p>
            </div>
        </div>
        <hr>
    </div>
</section>
<!-- END Promo #3 -->

<!-- Promo #4 -->
<section class="site-content site-section site-slide-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-5 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInRight" data-element-offset="-180">
                <h3 class="h2 site-heading site-heading-promo"></h3>
                <p class="promo-content"> <a href="features.php">Learn More..</a></p>
            </div>
            <div class="col-sm-6 col-md-offset-1 site-block visibility-none" data-toggle="animation-appear" data-animation-class="animation-fadeInLeft" data-element-offset="-180">
                <img src="img/placeholders/screenshots/" alt="Promo #4" class="img-responsive">
            </div>
        </div>
        <hr>
    </div>
</section>
<!-- END Promo #4 -->

<!-- Testimonials -->
<section class="site-content site-section">
    <div class="container">
        <!-- Testimonials Carousel -->
        <div id="testimonials-carousel" class="carousel slide carousel-html" data-ride="carousel" data-interval="4000">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#testimonials-carousel" data-slide-to="0" class="active"></li>
                <li data-target="#testimonials-carousel" data-slide-to="1"></li>
                <li data-target="#testimonials-carousel" data-slide-to="2"></li>
            </ol>
            <!-- END Indicators -->

            <!-- Wrapper for slides -->
            <div class="carousel-inner text-center">
                <div class="active item">
                    <p>
                        <img src="img/placeholders/avatars/avatar12.jpg" alt="Avatar" class="img-circle">
                    </p>
                    <blockquote class="no-symbol">
                        <p></p>
                        <footer><strong>Sophie Illich</strong>, example.com</footer>
                    </blockquote>
                </div>
                <div class="item">
                    <p>
                        <img src="img/placeholders/avatars/avatar7.jpg" alt="Avatar" class="img-circle">
                    </p>
                    <blockquote class="no-symbol">
                        <p></p>
                        <footer><strong>David Cull</strong>, example.com</footer>
                    </blockquote>
                </div>
                <div class="item">
                    <p>
                        <img src="img/placeholders/avatars/avatar9.jpg" alt="Avatar" class="img-circle">
                    </p>
                    <blockquote class="no-symbol">
                        <p></p>
                        <footer><strong>Nathan Brown</strong>, example.com</footer>
                    </blockquote>
                </div>
            </div>
            <!-- END Wrapper for slides -->
        </div>
        <!-- END Testimonials Carousel -->
    </div>
</section>
<!-- END Testimonials -->

<!-- Sign Up Action -->
<section class="site-content site-section site-section-light themed-background-dark-night">
    <div class="container">
       
    </div>
</section>
<!-- END Sign Up Action -->

<!-- Quick Stats -->
<section class="site-content site-section themed-background">
    <div class="container">
        <!-- Stats Row -->
        <!-- CountTo (initialized in js/app.js), for more examples you can check out https://github.com/mhuggins/jquery-countTo -->
        <div class="row" id="counters">
            <div class="col-sm-4">
                <div class="counter site-block">
                    <span data-toggle="countTo" data-to="6800" data-after="+"></span>
                    <small>Attorneys</small>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="counter site-block">
                    <span data-toggle="countTo" data-to="5500" data-after="+"></span>
                    <small>HClients</small>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="counter site-block">
                    <span data-toggle="countTo" data-to="100" data-after="+"></span>
                    <small>New Accounts Today</small>
                </div>
            </div>
        </div>
        <!-- END Stats Row -->
    </div>
</section>
<!-- END Quick Stats -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<?php include 'inc/template_end.php'; ?>